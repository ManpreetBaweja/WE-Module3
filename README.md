# WE Module-3 : Gen-AI
Coursework for the Module 3 of the Women Engineers (WE) Program


## Assignment 1
Solved a non-trivial problem statement from our computational thinking course with the help of genAI (Gemini)

## Assignment 2
Used a genAI tool (Gemini) to learn the game of Yahtzee and played a game with it. Got the approach on how to write code for the Yahtzee game in Python from Gemini. No code was written.

## Assignment 3
Wrote the code and created testing strategies for a Yahtzee game using a genAI tool (Gemini)

## Assignment 4
Learnt about and implemented Markov chains for text generation using a genAI tool (Gemini)

## Assignment 5
Learnt about the 'Diamonds' card game and looked at game strategies using a genAI tool (Gemini). Wrote a report on this interaction.

## Status
Ongoing